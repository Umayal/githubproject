# Pull base image
FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install make & gcc
RUN apt update
RUN apt install make
RUN apt install libpq-dev gcc -y

# Install dos2unix
RUN apt install dos2unix -y

# Set work directory
WORKDIR /app

# Copy Req.txt
COPY req.txt /app/

# Install Requirements
RUN pip install -r req.txt

# Copy the code
COPY . /app/

# Expose Django Port
EXPOSE 8010

# Copy the entrypoint script
COPY entry.sh /entry.sh

# Making sure the script is in unix format
RUN dos2unix /entry.sh

# Fix script Permissions
RUN chmod +x /entry.sh

# Entrypoint
ENTRYPOINT ["/entry.sh"]
