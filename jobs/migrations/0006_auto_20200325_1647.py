# Generated by Django 3.0.4 on 2020-03-25 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0005_auto_20200325_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobs',
            name='company',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='jobs',
            name='location',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='jobs',
            name='title',
            field=models.TextField(),
        ),
    ]
