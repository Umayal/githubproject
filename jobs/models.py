from django.db import models


class Jobs(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    job_type_choices = [
        ('FT', 'Full Time'),
        ('PT', 'Part Time'),
        ('CO', 'Contract'),
    ]
    type = models.CharField(choices=job_type_choices, default='FT', max_length=2)
    url = models.URLField(null=True, blank=True, max_length=255)
    created_at = models.DateField()
    company = models.TextField(null=True, blank=True)
    location = models.TextField(null=True, blank=True)
    title = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    how_to_apply = models.TextField(null=True, blank=True)
    company_logo = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Jobs"
        verbose_name = "Job"

    def __str__(self):
        return self.title



