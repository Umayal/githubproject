from datetime import datetime
import requests
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Jobs
from .serializers import JobsSerializer
from rest_framework import generics, filters


@api_view(['GET', 'POST'])
def sync_jobs(request):
    """
    Fetching records from GitHub job API URL and storing it in DB
    :param request: None
    :return: Inserted Job records
    """
    try:
        for i in range(8):
            request_headers = {'Content-Type': 'application/json; charset=utf-8'}
            url = 'https://jobs.github.com/positions.json?page={}'.format(i)
            api_call = requests.get(url, headers=request_headers)
            api_call = api_call.json()
            if bool(api_call):
                print("URL:" + url)
                for data in api_call:
                    job_id = data['id']
                    try:
                        job_cursor = Jobs.objects.get(pk=job_id)
                        print(job_cursor.title + " Already Exist")
                        continue
                    except Jobs.DoesNotExist:
                        job_type = data['type']
                        if job_type == "Full Time":
                            job_type = "FT"
                        elif job_type == "Part Time":
                            job_type = "PT"
                        elif job_type == "Contract":
                            job_type = "CO"
                        url = data['url']
                        created_at = data['created_at']
                        create_at_time_object = datetime.strptime(created_at, '%a %b %d %H:%M:%S %Z %Y')
                        company = data['company']
                        location = data['location']
                        title = data['title']
                        description = data['description']
                        how_to_apply = data['how_to_apply']
                        company_logo = data['company_logo']
                        jobs = Jobs(id=job_id, type=job_type, url=url, created_at=create_at_time_object,
                                    company=company, location=location, title=title, description=description,
                                    how_to_apply=how_to_apply, company_logo=company_logo)
                        print(jobs.company)
                        try:
                            jobs.save()
                        except Exception as e:
                            print("e: ", e)
                            continue
            else:
                break
    except Exception as ex:
        print("ex:", ex)
    return Response("Success")


class SearchAPIView(generics.ListAPIView):
    search_fields = ['title', 'location']
    filter_backends = (filters.SearchFilter,)
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer


class FilterAPIView(generics.ListAPIView):
    serializer_class = JobsSerializer

    def get_queryset(self):
        """
        List jobs with filter
        :param: location, title, None
        :return: Filtered list of jobs
        """
        location = self.request.query_params.get('location', None)
        title = self.request.query_params.get('title', None)
        if location:
            job_cursor = Jobs.objects.filter(location__contains=location)
        elif title:
            job_cursor = Jobs.objects.filter(title__contains=title)
        else:
            job_cursor = Jobs.objects.filter()
        return job_cursor

