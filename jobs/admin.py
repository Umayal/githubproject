from django.contrib import admin
from .models import Jobs
# Register your models here.


class JobsAdmin(admin.ModelAdmin):
    search_fields = ('id', )


admin.site.register(Jobs, JobsAdmin)
