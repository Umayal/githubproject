from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('sync', views.sync_jobs),
    path('search', views.SearchAPIView.as_view()),
    url('jobs/', views.FilterAPIView.as_view())
]
